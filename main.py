# my First commit
TIC_TAC_TOE_BOARD = ["X",1,2,3,4,5,6,7,8]

def print_board():
  #0 1 2
  #3 4 5
  #6 7 8
  print("-------------------")
  print("Current Tic Tac Toc board: ")
  # print from 0 to 2 (slices)
  print(TIC_TAC_TOE_BOARD[0:3]) # slice will not take the number added to the right of the colon

  # print from 3 to 5
  print(TIC_TAC_TOE_BOARD[3:6])

  # print from 6 to 8
  # print(TIC_TAC_TOE_BOARD[6:]) # if we don't have a number to the right of the column, will take all the elements remaining
  print(TIC_TAC_TOE_BOARD[6:9])
  print("-------------------")

def player_move(player_symbol):
  # player_mark will be either "X" or "O"
  # input is always going to return a string
  player_position = input(f"Please indicate the position to play as {player_symbol} : ") # how to have variable in string?

  # validate that the user input is a number an is withing 0 to 8 range
  if (player_position == "0" or player_position == "1" or
      player_position == "2" or player_position == "3" or
      player_position == "4" or player_position == "5" or
      player_position == "6" or player_position == "7" or
      player_position == "8"):
    print("Valid choice!")
    player_position = int(player_position)

    # validate if the position is already taken
    if TIC_TAC_TOE_BOARD[player_position] == "X" or TIC_TAC_TOE_BOARD[player_position] == "O":
      print("Error: The position is already taken!")
      player_move(player_symbol)

    # we need to remove the elemnt from the board
    # print("*********** Debugging")
    # print(f"Player position value: {player_position}")
    # print(f"Board value: {TIC_TAC_TOE_BOARD}")
    # print("*********** Debugging")
    TIC_TAC_TOE_BOARD.pop(player_position)
    #insert need to take an index, the index is always an integer
    TIC_TAC_TOE_BOARD.insert(player_position, player_symbol)
  else:
    print("Eror!")
    # logic to ask the player to enter a new value until we have a valid input
    # print error and then call the function again to get a new user input
    player_move(player_symbol)

print_board()
player_move(player_symbol="O")

while True:
  # add something that will change the symbom from "O" to "X" on every loop
  # validation 4.
  player_move(player_symbol="O")

# 1. Validating that is a number [check]
# 2. Position is not already taken [check]
# 3. Validate that the number is between 0 and 8 [check]
# 4. Validate that switching players is working as expected [pending]

print_board()